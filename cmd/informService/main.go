package main

import (
	"bitbucket.org/airenas/listgo/internal/app/inform"
)

func main() {
	inform.Execute()
}
