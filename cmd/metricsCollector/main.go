package main

import "bitbucket.org/airenas/listgo/internal/app/metrics"

func main() {
	metrics.Execute()
}
