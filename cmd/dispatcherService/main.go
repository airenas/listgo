package main

import "bitbucket.org/airenas/listgo/internal/app/dispatcher"

func main() {
	dispatcher.Execute()
}
